﻿using PetStores.XunitProject.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace PetStores.XunitProject.Tests
{
    public class PetStoreTests
    {
        private readonly ITestOutputHelper output;

        public PetStoreTests(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Get_PetName()
        {
            new ServiceWorkFlow(output).Validate_GetPetNames();
        }

        //Esse aqui
        [Fact]
        [Trait("Category", "petStore")]
        public void CreatPet_PostMethod()
        {
            new ServiceWorkFlow(output).ValidateCreatePetHasAdded(CustomConfigurationProvider.GetSection($"AddPETAPI"));
        }
    }
}
