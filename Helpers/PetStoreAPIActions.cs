﻿using Newtonsoft.Json;
using PetStores.XunitProject.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text.Json;
using Xunit.Abstractions;

namespace PetStores.XunitProject.Helpers
{
    public class PetStoreAPIActions
    {
        //Logger file to log the messages
        private ITestOutputHelper LoggerOutput;
        public ITestOutputHelper Logger
        {
            get
            {
                return LoggerOutput;
            }
            set
            {
                LoggerOutput = value;
            }
        }

        public PetStoreAPIActions(ITestOutputHelper output)
        {
            this.LoggerOutput = output;
        }

        public GetPetNames_Response Get_PetNames()
        {
            //Create a RestClient
            RestClient restClient = new RestClient();
            RestRequest restRequest = new RestRequest(Method.GET);
            IRestResponse restResponse;

            //create base url
            restClient.BaseUrl = new Uri(PetStoreAPIMethods.GetPetName);
            restResponse = restClient.Execute(restRequest);

            //Response Validation
            if (restResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<GetPetNames_Response>(restResponse.Content);
            }
            else
            {
                LoggerOutput.WriteLine("Response is null");
                return null;
            }
        }











        public CreatePet_Response Validate_AddPet(CreatePet_Request createPetRequest)
        {
            //Creating a RestClient
            RestClient restClient = new RestClient();
            RestRequest restRequest = new RestRequest(Method.POST);// Since this is a post method Method.post
            IRestResponse restResponse;

            //We need to create request base url
            restClient.BaseUrl = new Uri(PetStoreAPIMethods.AddNewPet);
            //serializing and sending the data
            var request = JsonConvert.SerializeObject(createPetRequest.createPetRequestObjectProperty);
            restRequest.AddJsonBody(request);

            //now execute the above request
            restResponse = restClient.Execute(restRequest);
            //Now validate the staus of the response

            if (restResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                //Deserialize the response from json and return the response object
                return JsonConvert.DeserializeObject<CreatePet_Response>(restResponse.Content);
            }
            else
            {
                LoggerOutput.WriteLine("Not a valid input" + restResponse.StatusCode);
                return null;
            }

        }

        public string Get_PetDetails_ByPetID(string id)
        {
            //Creating a RestClient
            RestClient restClient = new RestClient();
            RestRequest restRequest = new RestRequest(Method.POST);// Since this is a post method Method.post
            IRestResponse restResponse;

            //We need to create request base url, we need to pass pet id here
            var url = PetStoreAPIMethods.GetPetByID.Replace("{id}", id);
            restClient.BaseUrl = new Uri(url);
            restResponse = restClient.Execute(restRequest);

            //Validate the response
            if (restResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                //Returning the request object, When we execute the Get method we get Pet Created response.
                return "Pet has been created";
            }
            else
                return null;

        }
    }
}
