﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetStores.XunitProject.Helpers
{
    class PetStoreAPIMethods
    {

        public static string AddNewPet
        {
            get
            {
                return $"{CustomConfigurationProvider.GetKey("petStoreService-baseUrl")}" + "pet";
            }
        }

        public static string GetPetByID
        {
            get
            {
                return $"{CustomConfigurationProvider.GetKey("petStoreService-baseUrl")}" + "v2/pet/{id}";
            }
        }

        public static string GetPetName
        {
            get
            {
                return $"{CustomConfigurationProvider.GetKey("petStoreService-baseUrl")}" + "/pet/test";
            }
        }
    }
}
