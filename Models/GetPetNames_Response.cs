﻿using System.Text.Json.Serialization;


namespace PetStores.XunitProject.Models
{
    public class GetPetNames_Response
    {
        [JsonPropertyName("id")]
        public int id { get; set; }

        [JsonPropertyName("animal")]
        public Animal animal { get; set; }
    }

    public class Animal
    {
        public string nome { get; set; }
        public int idade { get; set; }
    }
}
