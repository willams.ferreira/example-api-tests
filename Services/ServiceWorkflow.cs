﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PetStores.XunitProject.Helpers;
using PetStores.XunitProject.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace PetStores.XunitProject.Services
{
    public class ServiceWorkFlow
    {
        public readonly ITestOutputHelper LoggerOutput;


        public ServiceWorkFlow(ITestOutputHelper logger)
        {
            this.LoggerOutput = logger;
        }

        public void Validate_GetPetNames()
        {
            var response = new PetStoreAPIActions(LoggerOutput).Get_PetNames();

            if (response != null)
            {
                //Result Validation
                Assert.Equal(1, response.id);

                //Type Validation
                Assert.Equal(string.Empty.GetType(), response.animal.nome.GetType());
            }
            else
            {
                LoggerOutput.WriteLine("Test Failed");
                Assert.NotNull(response);
            }
        }

        
        
        
        
        
        
        
        
        
        
        
        
        public void ValidateCreatePetHasAdded(object jsonInput)
        {
            var data = JObject.Parse(jsonInput.ToString()); // parsing the json in to request object property
            var requests = new CreatePet_Request();
            CreatePet_RequestObject objectRequest = new CreatePet_RequestObject();
            objectRequest = JsonConvert.DeserializeObject<CreatePet_RequestObject>(jsonInput.ToString());
            objectRequest.id = int.Parse(data["id"].ToString()); // since id is in Integer we need to parse json string to integer
            objectRequest.name = data["name"].ToString();

            //To add Category. Category is of object type so we need to add them like this
            Category category = new Category();
            category.id = int.Parse(data["category"]["id"].ToString());
            category.name = data["category"]["name"].ToString();

            objectRequest.category = category;
            //Photo urls is of type array
            for (int i = 0; i < objectRequest.photoUrls.Count; i++)
            {
                var photeUrls = objectRequest.photoUrls[i].ToString();
            }
            for (int i = 0; i < objectRequest.tags.Count; i++)
            {
                var tagID = objectRequest.tags[i].id.ToString();
                var tagName = objectRequest.tags[i].name.ToString();
            }

            objectRequest.status = data["status"].ToString();

            //Assigning all the data from objectRequest to request object property
            requests.createPetRequestObjectProperty = objectRequest;

            //send the request to API post method to execute and get the response back from the request API
            var responseObject = new PetStoreAPIActions(LoggerOutput).Validate_AddPet(requests);


            if (responseObject != null)
            {
                //Validate response data
                Assert.True(responseObject.Id == objectRequest.id, "Id is matching");
                Assert.True(responseObject.Name == objectRequest.name, "Name is matching");
                Assert.True(responseObject.Status == objectRequest.status, "status is matching");
                //Tags response comparision
                for (int i = 0; i < responseObject.Tags.Length; i++)
                {
                    Assert.True(responseObject.Tags[i].id == objectRequest.tags[i].id, "Tag ID is matching");
                    Assert.True(responseObject.Tags[i].name == objectRequest.tags[i].name, "tag Name is matching");
                }
                //photourls
                for (int i = 0; i < responseObject.PhotoUrls.Length; i++)
                {
                    Assert.True(responseObject.PhotoUrls[i].ToString() == objectRequest.photoUrls[i].ToString(), "Photo urls is matching");
                }

            }


        }

        public bool Validate_GetPetResponse(string id, object jsonInput)
        {
            var data = JObject.Parse(jsonInput.ToString());
            var response = new PetStoreAPIActions(LoggerOutput).Get_PetDetails_ByPetID(id);

            if (response.Equals("Pet has been created"))
                return true;
            else
                return false;
        }
    }
}
